'''
2016年3月2日

微拓科技-张志波
'''
from init import PATH
import configparser
import sys

#读取配置文件
conf = configparser.ConfigParser()
conf.read(PATH()+'init.conf')
#读取显示器类型
networks_type=conf.get('display','type')
#如果为lcd12864
if(networks_type=='lcd12864'):
    import display.lcd12864 
    display=lcd12864.Display()
#如果为hdmi
if(networks_type=='hdmi'):
    import display.hdmi
    display=hdmi.Display()