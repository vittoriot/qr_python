'''
2016年3月2日

微拓科技-张志波
'''
from init import PATH
import qrcode
from tkinter import *

class Hdmi:
    def __init__(self):
        self.root = Tk()
        self.size = 3
    def start(self,monitore_data):
        #生成一个背景为白色，高480像素、宽800像素的背景
        #Canvas(self.root, bg="white", height=480, width=800).pack()
        #生成一个高480像素、宽800像素窗体
        self.root.configure(background='white')
        self.root.geometry('800x480')
        #设置窗口大小可拖拽
        self.root.resizable(width=True, height=True)
        #二维码图片位置
        QRCode_image=PhotoImage(file = monitore_data)
        label = Label(self.root,image = QRCode_image)
        label.QRCode_image = QRCode_image
        label.place(relx= 0.23, rely=0.03)
        #使得窗口全屏
        self.root.attributes("-fullscreen", True)
        self.root.after(100,self.root.update())
class Conver:
    def __init__(self):
        self.size=3
        self.PATH=PATH()+"display/QRCode_image/qrcode.png"
    def bin(self,text):
        #初始化二进制字符串
        qrc_2=''
        qr = qrcode.QRCode(
            #范围为1到40，表示二维码的大小,如果想让程序自动生成，将值设置为 None 并使用 fit=True 参数即可。
            version=self.size,
            #二维码的纠错范围，可以选择4个常量
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            #每个点的像素个数
            box_size=15,
        )
        qr.add_data(text)
        #以字符形式输出
        qr_image = qr.make_image()
        qr_image.save(self.PATH)
        return(self.PATH)
class Display:
    def __init__(self):
        self.hdmi = Hdmi()
    def startDraw(self,monitore_data):
        self.hdmi.start(Conver().bin(monitore_data))
