'''
2016年3月2日

微拓科技-张志波
'''
from init import PATH
import time
import sys
import os
import hashlib
import configparser
sys.path.append(PATH()+'lock')
import lock
from socket import *

#读取配置文件
conf = configparser.ConfigParser()
conf.read(PATH()+'init.conf')

#获取MAC地址 当前设置为设备ID
def get_mac_address(): 
    mac=os.popen('ifconfig eth0 | grep "HWaddr" | awk \'{print $5}\'').read().encode('utf-8')
    md5=hashlib.md5()
    md5.update(mac)
    mac_md5=md5.hexdigest()
    return mac_md5[0:10]
#网卡类
class Networks:
    def __init__(self):
        #设备ID
        self.devId=get_mac_address()
        #获取url
        self.url=conf.get('networks','url')
        #获取port
        self.port=conf.getint('networks','port')
    """
    网络设备初始化类
    参数：
    status 默认/0->判断wifi初始化是否完成 -1->恢复到未初始化状态 1->开始初始化 
    仅当status->1时有下面参数传递
        ssid wifi名字
        password wifi密码
    返回值：
    status-> 0时   Boolean wifi初始化完成/未完成 True/False
    status->-1时   Boolean wifi恢复初始化完成/未完成 True/False
    status-> 1时   Boolean wifi初始化设置成功/未成功 True/False
    """
    def deviceInit(self,status=0,ssid="",password=""):
        try:
            #检查设备wifi是否已经初始化
            inited=conf.getint('inited','wifiInited')
            if(inited==0 or status==-1):
                #打开wifi配置文件
                wifi_config=open('/etc/wpa_supplicant/wpa_supplicant.conf','w')
                print('start wifi init , status -1')
                wifi_config_info=(  'network={\n'+
                                    'ssid="'+self.devId+'"\n'+
                                    'key_mgmt=WPA-PSK\n'+
                                    'psk="'+self.devId+'"\n'+
                                    #wifi优先级
                                    'priority=1\n'+
                                    '}\n')
                wifi_config.write(wifi_config_info)
                wifi_config.close();
                #设置配置文件
                conf.set('inited','wifiInited','1')
                conf.write(open(PATH()+'init.conf',"w"))
                print('start wifi pass , status -1')
                #重启程序
                while 1:
                    os.system('echo reboot')
                    time.sleep(1)
                    os.system('reboot')
                    time.sleep(1)
            #查询wifi是否已经初始化
            if(status==0):
                if(inited==1):
                    return self.devId
                elif(inited==2):
                    return True
                #状态异常
                else:
                    #设置配置文件
                    conf.set('inited','wifiInited','0')
            #设置wifi帐号密码
            if(status==1):
                #打开wifi配置文件
                wifi_config=open('/etc/wpa_supplicant/wpa_supplicant.conf','w')
                print('start wifi init , status 1')
                wifi_config_info=(  'network={\n'+
                                    'ssid="'+self.devId+'"\n'+
                                    'key_mgmt=WPA-PSK\n'+
                                    'psk="'+self.devId+'"\n'+
                                    #wifi优先级
                                    'priority=1\n'+
                                    '}\n'+
                                    'network={\n'+
                                    'ssid="'+ssid+'"\n'
                                    'key_mgmt=WPA-PSK\n'+
                                    'psk="'+password+'"\n'+
                                    'priority=100\n'+
                                    '}\n')
                wifi_config.write(wifi_config_info)
                wifi_config.close();
                #设置配置文件
                conf.set('inited','wifiInited','2')
                conf.write(open(PATH()+'init.conf',"w"))
                print('start wifi pass , status 1')
                #重启程序
                while 1:
                    os.system('echo reboot')
                    time.sleep(1)
                    os.system('reboot')
                    time.sleep(1)
        except Exception as e:
            print('wifi init error:'+str(e))
            time.sleep(0.1)
            return (False)
    """
    socket初始化
    返回值：
    Boolean 初始化成功/失败 True/False
    """
    def socketInit(self):
        try:
            print('start init socket')
            #实例化socketClient类
            self.socketClient=SocketClient(self.url,self.port)
            print('init socket pass')
            return (True)
        except Exception as e:
            print('init socket error:'+str(e))
            time.sleep(0.1)
            return (False)
    """
    参数:
    state 状态码
    QRC   二维码请求状态
    返回值：
    Boolean 发送成功/失败 True/False
    """
    def socketSend(self,data):
        try:
            print("start send")
            #创建发送socket请求
            self.socketClient.socketSend(data)
            print('send socket pass')
            return (True)
        except Exception as e:
            print('send socket error:'+str(e))
            time.sleep(0.1)
            return (False)
    """
    参数:
    返回值：
    String  接受成功 接收到的消息
    Boolean 接受失败 False
    """
    def socketGet(self):
        try:
            print("start get")
            #创建接收socket请求
            socketGetMessage=self.socketClient.socketGet()
            #将收到的信息写入日志中
            print("get socket pass:"+socketGetMessage)
            return (socketGetMessage)
        except Exception as e:
            print('get socket error:'+str(e))
            time.sleep(0.1)
            return(False)

#Socket客户端类
class SocketClient:
        """
        类初始化
        参数：
        host socket连接地址
        port socket连接端口
        """
        def __init__(self,host,port):
                #连接本机
                self.HOST=host
                #设置侦听端口
                self.PORT=port
                #设置字节长度
                self.BUFSIZ=1024
                #设置地址
                self.ADDR=(self.HOST,self.PORT)
                #设置客户端
                self.client=socket(AF_INET, SOCK_STREAM)
                #设置超时时间
                self.client.settimeout(conf.getint('networks','timeout'))
                self.client.connect(self.ADDR)
        """
        发送socket请求
        参数:
        state 命令状态码
        devId 设备ID
        QRC   二维码请求状态
        """
        def socketSend(self,data):
            #python3传递的是bytes，所以要编码
            self.client.send(data.encode('utf8'))
        """
        获取socket消息
        返回值：
        String 接收到的消息
        """
        def socketGet(self):
            data=self.client.recv(self.BUFSIZ).decode('utf8')
            #对信息进行处理
            return data;