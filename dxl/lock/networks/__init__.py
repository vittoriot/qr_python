'''
2016年3月2日

微拓科技-张志波
'''
from init import PATH
import sys
import configparser

#读取配置文件
conf = configparser.ConfigParser()
conf.read(PATH()+'init.conf')
#读取网卡配置
networks_type=conf.get('networks','type')
#如果为wifi
if(networks_type=='wifi'):
    import networks.wifi 
    networks=wifi.Networks()