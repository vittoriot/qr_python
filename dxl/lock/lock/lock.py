'''
2016年3月2日

微拓科技-张志波
'''
import RPi.GPIO as GPIO
import time
import configparser
import sys
from init import PATH

GPIO.setmode(GPIO.BCM)
#开锁
def open(status=0):
    #读取配置文件
    conf = configparser.ConfigParser()
    conf.read(PATH()+'init.conf')
    #获取引脚 Pin
    lockPin=conf.getint('lock','switch')
    if(status==0):
        #开门
        if(conf.getint('lock','openModel')==2):
            GPIO.setup(lockPin,GPIO.IN)
        elif(conf.getint('lock','openModel')==0):
            GPIO.setup(lockPin,GPIO.OUT)
            GPIO.output(lockPin,GPIO.LOW)
        elif(conf.getint('lock','openModel')==1):
            GPIO.setup(lockPin,GPIO.OUT)
            GPIO.output(lockPin,GPIO.HIGH)
        time.sleep(conf.getint('lock','sleep'))
        if(conf.getint('lock','closeModel')==2):
            GPIO.setup(lockPin,GPIO.IN)
        elif(conf.getint('lock','closeModel')==0):
            GPIO.setup(lockPin,GPIO.OUT)
            GPIO.output(lockPin,GPIO.LOW)
        elif(conf.getint('lock','closeModel')==1):
            GPIO.setup(lockPin,GPIO.OUT)
            GPIO.output(lockPin,GPIO.HIGH)
    if(status==1):
        disconnectedOpened=conf.getint('lock','disconnectedOpened')
        if(disconnectedOpened==1):
            if(conf.getint('lock','openModel')==2):
                GPIO.setup(lockPin,GPIO.IN)
            elif(conf.getint('lock','openModel')==0):
                GPIO.setup(lockPin,GPIO.OUT)
                GPIO.output(lockPin,GPIO.LOW)
            elif(conf.getint('lock','openModel')==1):
                GPIO.setup(lockPin,GPIO.OUT)
                GPIO.output(lockPin,GPIO.HIGH)
    print("open:"+str(status))
#关锁
def shut():
    #读取配置文件
    conf = configparser.ConfigParser()
    conf.read(PATH()+'init.conf')
    #获取引脚 Pin
    lockPin=conf.getint('lock','switch')
    if(conf.getint('lock','closeModel')==2):
            GPIO.setup(lockPin,GPIO.IN)
    elif(conf.getint('lock','closeModel')==0):
        GPIO.setup(lockPin,GPIO.OUT)
        GPIO.output(lockPin,GPIO.LOW)
    elif(conf.getint('lock','closeModel')==1):
        GPIO.setup(lockPin,GPIO.OUT)
        GPIO.output(lockPin,GPIO.HIGH)
    print("shut")
#检测门禁状态
def check():
    #读取配置文件
    conf = configparser.ConfigParser()
    conf.read(PATH()+'init.conf')
    #获取引脚 Pin
    checkPin=conf.getint('lock','check')
    GPIO.setup(checkPin,GPIO.IN)
    input_gpio=GPIO.input(checkPin)
    return input_gpio