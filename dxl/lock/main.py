'''
2016年3月2日

微拓科技-张志波
'''
import configparser
import sys
import time
from init import PATH
from networks import networks
from display import display
sys.path.append(PATH()+'message')
from message import message
sys.path.append(PATH()+'lock')
import lock

#读取配置文件
conf = configparser.ConfigParser()
conf.read(PATH()+'init.conf')
#二维码显示的链接地址
qr_url=conf.get('networks','url')
#二维码显示的端口
qr_port=conf.get('networks','qrPort')
def main():
    while 1:
        try:
            print('start check device init')
            lock.open(1)
            #检测检测设备是否初始化过
            deviceInited=networks.deviceInit()
            if(deviceInited!=True):
                dev_id=deviceInited
                #显示二维码
                display.startDraw("http://"+qr_url+":"+qr_port+"?a=@"+dev_id+"@noinit#")
                print ("http://"+qr_url+":"+qr_port+"?a=@"+dev_id+"@noinit#")
            print('check device init pass')
            print('start run')
        except Exception as e:
            print('main.py check device init error:'+str(e))
        while 1:
            try:
                lock.open(1)
                #初始化网络连接
                if(networks.socketInit()==True):
                    if(deviceInited!=True):
                        send_data="#1@"+networks.devId+"@1#"
                    else:
                        send_data="#1@"+networks.devId+"@2#"
                    #网卡发送消息,注册和获取二维码
                    if(networks.socketSend(send_data)==True):
                        #记录上次请求二维码的时间
                        qrcode_timer = 0
                        while 1:
                            #网卡接收消息
                            data=networks.socketGet()
                            if(data!=False):
                                try:
                                    if data!=None:
                                        #关门
                                        lock.shut()
                                        #处理接受到的信息
                                        data_dispose=message(data)
                                        if(data_dispose[0]=='0'):
                                            print ('心跳')
                                        #显示二维码
                                        elif (data_dispose[0]=='1' or data_dispose[0]=='3'):
                                            qrcode_timer=int(time.time())
                                            if(data_dispose[0]=='3'):
                                                #开锁
                                                lock.open()
                                                print ("开锁")
                                            if(deviceInited!=True):
                                                #显示初始化二维码
                                                display.startDraw("http://"+qr_url+":"+qr_port+"?a=@"+data_dispose[2]+"@init@"+data_dispose[3]+"#")
                                                print ("二维码：http://"+qr_url+":"+qr_port+"?a=@"+data_dispose[2]+"@init@"+data_dispose[3]+"#")
                                            #带参数的二维码
                                            elif(data_dispose[1]=='2'):
                                                #显示二维码
                                                display.startDraw("http://weixin.qq.com/q/"+data_dispose[3])
                                                print ("二维码：http://weixin.qq.com/q/"+data_dispose[3])
                                            else:
                                                #显示二维码
                                                display.startDraw("http://"+qr_url+":"+qr_port+"?a=@"+data_dispose[2]+"@"+data_dispose[3]+"#")
                                                print ("二维码：http://"+qr_url+":"+qr_port+"?a=@"+data_dispose[2]+"@"+data_dispose[3]+"#")
                                        #直接开锁
                                        elif (data_dispose[0]=='2'):
                                            #开锁
                                            lock.open()
                                            print ('开锁')
                                        #更新WIFI配置信息
                                        elif(data_dispose[0]=='5'):
                                            display.startClear()
                                            networks.deviceInit(1,data_dispose[1],data_dispose[2])
                                        #二维码超时后重新请求二维码
                                        if(int(time.time())-qrcode_timer>3600):
                                            if(deviceInited!=True):
                                                networks.socketSend("#1@"+networks.devId+"@1#")
                                            else:
                                                networks.socketSend("#1@"+networks.devId+"@2#")
                                        else:
                                            #发送心跳包
                                            networks.socketSend("#0@#")
                                except Exception as e:
                                        print('main.py socket error:'+str(e))
                                        break
                            else:
                                break
            except Exception as e:
                print('main.py networks init error'+str(e))
if __name__ == '__main__':
    main()
